# admina: An open-source administration tool for Apache Cassandra #
**Apache Cassandra** is a massively scalable open source NoSQL database. Cassandra is perfect for
managing large amounts of structured, semi-structured, and unstructured data across multiple data centers
and the cloud. Cassandra delivers continuous availability, linear scalability, and operational simplicity
across many commodity servers with no single point of failure, along with a powerful dynamic data model
designed for maximum flexibility and fast response times.

**admina** is developed for easy administration of Apache Cassandra. Multiple cluster needs proper concentration and administration.
A cluster consists of several nodes are tough to manage manually, an admin UI can make an administrators life much more easier.

### INSTALLATION ###
#### install django ####
`pip install django==1.11`

#### install screen ####
`yum -y install screen`

1. change ALLOWED_HOSTS = ['XXX.XXX.XXX.XXX'] in cassmonitor/settings.py
2. comment 'django_cassandra_engine' in cassmonitor/settings.py

#### run application ####
`screen python manage.py runserver xxx.xxx.xxx.xxx:8001`