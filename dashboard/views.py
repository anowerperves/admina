from django.shortcuts import render
import subprocess
import commands
import os
import re
def index(request):
	#version = subprocess.check_output(['cassandra', '-v'])
	#version = commands.getstatusoutput("cassandra -v")
	#version1 = type(version)
	#print "version : %s"%version1
	#version = os.popen("cassandra -v").read()
	version = execshell("cassandra -v")
	#version = "2.2.4"
	#print version
	cluster_name = execshell("nodetool -u cassandra -pw cassandra describecluster | grep Name | awk '{print $2}'")
	node_count = execshell("nodetool -u cassandra -pw cassandra ring | awk '{print $1}' | sort | uniq -d | grep 192 | wc -l")
	up_node = execshell("nodetool -u cassandra -pw cassandra status | grep UN | wc -l")
	down_node = execshell("nodetool -u cassandra -pw cassandra status | grep DN | wc -l")
	down_list = ""
        datasize = "###"
        datasize = execshell("nodetool -u cassandra -pw cassandra status | grep UN |awk 'END{print $3}'")
        data_param = "##"
        data_param = execshell("nodetool -u cassandra -pw cassandra status | grep UN |awk 'END{print $4}'")
        if cluster_name == '':
            cluster_name = "CASSANDRA IS NOT RUNNING!"
	    version = "start cassandra to see version"
	    node_count = '#'
            up_node = '#'
            down_node = '#'
	if down_node != '' and down_node != '0':
	    down_list = execshell("nodetool -u cassandra -pw cassandra status | grep DN | awk '{print $2}'")
	    down_list = ', '.join(re.findall('\S+', down_list))
	warn_log = execshell("cat /opt/apache-cassandra-2.2.4/logs/system.log | grep WARN | wc -l")
	warn_message = execshell("cat /opt/apache-cassandra-2.2.4/logs/system.log | grep WARN")
	error_log = execshell("cat /opt/apache-cassandra-2.2.4/logs/system.log | grep ERROR | wc -l")
	error_message = execshell("cat /opt/apache-cassandra-2.2.4/logs/system.log | grep ERROR")
	return render(request, 'dashboard/index.html', {'version': version, 'cluster_name' : cluster_name, 'node_count': node_count, 'up_node': up_node, 'down_node': down_node, 'down_node_list': down_list, 'warn_log': warn_log, 'error_log': error_log, 'error_message': error_message, 'warn_message': warn_message, 'data_size': datasize, 'data_param': data_param})

def execshell(cmd):
	return os.popen(cmd).read()


def restart():
	node_count = execshell("nodetool -u cassandra -pw cassandra ring | awk '{print $1}' | sort | uniq -d | grep 192 | wc -l")
	up_node = execshell("nodetool -u cassandra -pw cassandra status | grep UN | wc -l")
	down_node = execshell("nodetool -u cassandra -pw cassandra status | grep DN | wc -l")
	error_log = execshell("cat /opt/apache-cassandra-2.2.4/logs/system.log | grep ERROR | wc -l")
	warn_log = execshell("cat /opt/apache-cassandra-2.2.4/logs/system.log | grep WARN | wc -l")
	return render(request, 'dashboard/restart.html', {'node_count': node_count, 'up_node': up_node, 'down_node': down_node, 'warn_log': warn_log, 'error_log': error_log})
	
